package io.oseberg.interview

import org.scalatest._
import scala.io._

class ScreenerSpec extends FlatSpec with Matchers {

  behavior of "FileStats"

  val source = Source.fromFile("./src/test/resources/DoI.txt")
  val fileStats = FileStats.gatherStats(source)

  it must "count the lines in a file" in {
    val expectedLines = 41
    fileStats.lineCount should be (expectedLines)
  }

  it must "count all the words in a file" in {
    val expectedWords = 1335
    fileStats.totalWordCount should be (expectedWords)
  }

  it must "count occurences of a specific word in a file" in {
    fileStats.wordFreq("People") should be(10)
  }

  it must "calculate the frequency of all words in a file" in {
    // 'the' and 'of' have the same frequency. Testing against 'of, the, to' instead of 'the, of, to' since it's sorted
    fileStats.topXWordsByFreq(3) should be (Seq("of","the","to"))
  }
}