package io.oseberg.interview

import scala.io._

object Screener extends App {
  val source = Source.fromFile("./src/main/resources/DoI.txt")
  val stats = FileStats.gatherStats(source)

  println("Stats for file: DoI.txt:")
  println(s"Line Count: ${stats.lineCount}")
  println(s"Total Word Count: ${stats.totalWordCount}")
  println(s"Count for 'People': ${stats.wordFreq("People")}")

  // 'the' and 'of' have the same frequency. It will be printed as 'of, the, to' instead of 'the, of, to' since it's sorted
  println(s"Top Three Words by Freq: ${stats.topXWordsByFreq(3) mkString ", "}")
}

case class FileStat(lineCount: Int, totalWordCount: Int, wordCount: Map[String, Int]) {

  def wordFreq(word: String): Int = (wordCount collect {
    case (k, v) if k == word.toLowerCase => v
  }).headOption.getOrElse(0)

  def topXWordsByFreq(x: Int): Seq[String] = (wordCount.toVector.sortWith(_._2 > _._2) take x) map (_._1)
}

object FileStats {
  def gatherStats(source: Source): FileStat = {
    val lines: Seq[String] = source.getLines toSeq
    val words: Seq[String] = lines filterNot (_.isEmpty) flatMap (_.split(" "))
    val wordCount = words.groupBy(_.replaceAll("[^a-zA-Z0-9]", "").toLowerCase).mapValues(_.length)
    FileStat(lines.size, words.length, wordCount)
  }
}